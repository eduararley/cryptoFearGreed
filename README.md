Crypto Fear & Greed Index
=========================

Browser extension for [Chrome](https://www.google.com/chrome/) and derivate browsers, that shows the latest Crypto Fear & Greed Index, taken directly from [alternative.to](https://alternative.to) using its API.

## License

cryptoFearGreed is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.