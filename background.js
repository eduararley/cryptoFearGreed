/*
    This file is part of cryptoFearGreed.

    cryptoFearGreed is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3.

    cryptoFearGreed is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cryptoFearGreed.  If not, see <https://www.gnu.org/licenses/>.
*/

"use strict";

// alternative.to API endpoint
const url = "https://api.alternative.me/fng/";

// Create or update Chrome Alerts
var _updateRepeatingAlarms = function (period = 0) {
    chrome.alarms.clear("checkFGI",
        function () {
            if (period === 0) {
                chrome.alarms.create("checkFGI", {
                    when: Date.now()
                });
            } else {
                chrome.alarms.create("checkFGI", {
                    when: Date.now() + (parseInt(period) * 1000) + Math.floor((Math.random() * 240000) + 60000),
                    periodInMinutes: 1440
                });
            }
        }
    );
}

// Set badge text, hint and icon when API data is retrieved
var setFearGreedBadge = function (fgiValue, fgiClass, fgiNextUpd) {
    let redValue = fgiValue * 255 / 100;
    let greenValue = 255 - redValue;
    chrome.action.setBadgeText({ text: fgiValue.toString() });
    chrome.action.setTitle({ title: "Crypto Fear & Greed Index: " + fgiClass })
    let iconFileNum = Math.floor(fgiValue / 10) * 10;
    chrome.action.setIcon({ path: "icons/16_" + iconFileNum + ".png" });
    _updateRepeatingAlarms(fgiNextUpd);
}

// Query alternative.to API
var checkFGI = async function () {
    let response = await fetch(url);
    let fgi = await response.json();
    let index = fgi.data[0];
    setFearGreedBadge(index.value, index.value_classification, index.time_until_update);
}

// Alarms
function onAlarm(alarm) {
    switch (alarm.name) {
        case "checkFGI":
            checkFGI();
            break;
        default:
            break;
    }
}

// Trigger alarm setup on installed and startup
function onInstalled() { _updateRepeatingAlarms(); }
function onStartup() { _updateRepeatingAlarms(); }

// Listeners
chrome.runtime.onInstalled.addListener(onInstalled);
chrome.runtime.onStartup.addListener(onStartup);
chrome.alarms.onAlarm.addListener(onAlarm);